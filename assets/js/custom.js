$(function () {
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            $('li.active').removeClass('active');
            $(this).parent().addClass('active');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});

/**
 * downloadAsPDF    Trigers a PDF download event. The PDF will contain the contents of the
 *                  DOM element whose ID is passed as @contents.
 *
 * @param contents  ID of the element to be included in the PDF document.
 *
 * @name downloadAsPDF
 */
function downloadAsPDF(contents){
    /**
     * jsPDF: Creates new jsPDF document object instance.
     *
     * @param orientation   One of "portrait" or "landscape" (or shortcuts "p" (Default), "l")
     *
     * @param unit          Measurement unit to be used when coordinates are specified.
     *                      One of "pt" (points), "mm" (Default), "cm", "in".
     *
     * @param format        One of 'pageFormats' as shown below, default: a4
     *
     * @returns {jsPDF}
     *
     * @name jsPDF
     */
    var pdf = new jsPDF('p', 'pt', 'a4')

    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    , source = $('#' + contents)[0]

    // we support special element handlers. Register them with jQuery-style
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors
    // (class, of compound) at this time.
    , specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    },

    margins = {
        top: 40,
        bottom: 60,
        left: 40,
        width: 522
    };

    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
        source  // HTML string or DOM elem ref.
        , margins.left  // x coord
        , margins.top   // y coord
        , {
            'width': margins.width // max width of content on PDF
            , 'elementHandlers': specialElementHandlers
        },

        //dispose: object with X, Y of the last line add to the PDF
        //         this allow the insertion of new lines after html
        function (dispose) {
            pdf.save('biofuels.pdf');
        },
        margins
    );

}

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})


